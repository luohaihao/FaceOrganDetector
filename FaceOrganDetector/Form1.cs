﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Collections;
using System.Diagnostics;

namespace EyeDetector
{
    public partial class Form1 : Form
    {
        private Capture _capture;
        private HaarCascade _faces;
        private HaarCascade _faceyes;
        int H_MIN = 0;
        int H_MAX = 256;
        int S_MIN = 0;
        int S_MAX = 256;
        int V_MIN = 0;
        int V_MAX = 256;
        int graymin = 1;
        int graymax = 2;
        bool enableFlag= false;


        int xMax;
        int yMax;
        private BackgroundWorker updateCam;
        Rectangle globleROI_mouthRefer;

        public Form1()
        {

            InitializeComponent();
            _capture = new Capture(1);
            //_faces = new HaarCascade("E:\\haarcascade_frontalface_alt_tree.xml");
            _faces = new HaarCascade("E:\\haarcascade_frontalface_alt2.xml");
            // _faces = new HaarCascade("E:\\haarcascade_mcs_mouth (1).xml");
            _faceyes = new HaarCascade("E:\\haarcascade_sub.xml");
           // _faceyes = new HaarCascade("E:\\frontalEyes35x16.xml");
            
            //_eyes = new HaarCascade("E:\\haarcascade_mcs_mouth (1).xml");
            ;
            //_faces = new HaarCascade("haarcascade_frontalface_alt_tree.xml");
            //_eyes = new HaarCascade("haarcascade_eye.xml");
            //_capture.ImageGrabbed += FrameGrabber;
            _capture.Start();

            Application.Idle += new EventHandler(FrameGrabber);


            #region erode and dilate
            //Image<Bgr, Byte> erodeimg = new Image<Bgr, Byte>("E:\\gd2.bmp");
            // Image<Bgr, Byte> dilateimg= new Image<Bgr, Byte>("E:\\gd2.bmp");
            // CvInvoke.cvShowImage("Original picture", erodeimg);
            //CvInvoke.cvErode(erodeimg, erodeimg, IntPtr.Zero, 3);
            //CvInvoke.cvShowImage("Erode", erodeimg);
            //CvInvoke.cvDilate(dilateimg, dilateimg, IntPtr.Zero, 3);
            //CvInvoke.cvShowImage("Dilate", dilateimg);
            //pb_handled.Image = imgerondia.Bitmap;
            #endregion

        }

        void FrameGrabber(object sender, EventArgs e)
        {
            if(enableFlag)
            {

                 //We are acquiring a new frame
            //Capture ct = (Capture)sender;
            //Image<Bgr, Byte> frame = ct.RetrieveBgrFrame();  
            Image<Bgr, Byte> frame = _capture.QueryFrame().Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);
            //We convert it to grayscale
            Image<Gray, Byte> grayFrame = frame.Convert<Gray, Byte>();
            //Equalization step
            imageBoxCapturedFrame.Image = grayFrame;
            //imageBoxCapturedFrame.Image = frame;
            //CvInvoke.cvSmooth(grayFrame, grayFrame, Emgu.CV.CvEnum.SMOOTH_TYPE.CV_MEDIAN  , 3, 3, 0, 0);
            //CvInvoke.cvCanny(grayFrame, grayFrame, 1, 1, 3);
            grayFrame._EqualizeHist();
           // frame._EqualizeHist();
           // pb_handled.Image = grayFrame.Bitmap;
        

            Image<Bgr, Byte> preHSV = frame;
            var min = new Bgr(H_MIN, S_MIN, V_MIN);
            var max = new Bgr(H_MAX, S_MAX, V_MAX);
            Image<Hsv, Byte> imgHsv = preHSV.Convert<Hsv, Byte>();
            Image<Hsv, float> HSVimg = new Image<Hsv, float>(640,480);
            Image<Bgr, Byte> imageHSV = new Image<Bgr, Byte>(imgHsv.Bitmap);
            CvInvoke.cvCvtColor(imgHsv, imgHsv, COLOR_CONVERSION.CV_BGR2HSV);
            if (cb_HSV.Checked)
            {
                pb_HSV.Image = imgHsv.Bitmap;
            }
            else
                pb_HSV.Image = null;

            //pb_handled.Image = imgHsv.Bitmap;
            Image<Gray, Byte> imageHSVDest = imageHSV.InRange(min, max);
            if (cb_colorSelecter.Checked)
            {
                CvInvoke.cvShowImage("hsv", imageHSVDest);
            }
            else
            {
                CvInvoke.cvDestroyWindow("hsv");
            }


            Stopwatch watch;
            watch = Stopwatch.StartNew();
            // We assume there's only one face in the video
            watch.Start();

            //MCvAvgComp[][] facesDetected = grayFrame.DetectHaarCascade(_faces, 1.1, 1, Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.FIND_BIGGEST_OBJECT, new Size(20, 20));

            MCvAvgComp[][] facesDetected = grayFrame.DetectHaarCascade(_faces,1.1, 1, Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.FIND_BIGGEST_OBJECT, new Size(20, 20));

            //pb_handled.Image = grayFrame.Bitmap;
            if (facesDetected[0].Length == 1)
            {
                MCvAvgComp face = facesDetected[0][0];
                //Set the region of interest on the faces                

                #region Luca Del Tongo Search Roi based on Face Metric Estimation --- based on empirical measuraments on a couple of photos ---  a really trivial heuristic

                // Our Region of interest where find eyes will start with a sample estimation using face metric
                Int32 yCoordStartSearchEyes = face.rect.Top + (face.rect.Height * 3 / 11);
                Point startingPointSearchEyes = new Point(face.rect.X, yCoordStartSearchEyes);
                Point endingPointSearchEyes = new Point((face.rect.X + face.rect.Width), yCoordStartSearchEyes);

                Size searchEyesAreaSize = new Size(face.rect.Width, (face.rect.Height * 2 / 9));
                Point lowerEyesPointOptimized = new Point(face.rect.X, yCoordStartSearchEyes + searchEyesAreaSize.Height);
                Size eyeAreaSize = new Size(face.rect.Width / 2, (face.rect.Height * 2 / 9));
                Point startingLeftEyePointOptimized = new Point(face.rect.X + face.rect.Width / 2, yCoordStartSearchEyes);

                Rectangle possibleROI_eyes = new Rectangle(startingPointSearchEyes, searchEyesAreaSize);
                Rectangle possibleROI_rightEye = new Rectangle(startingPointSearchEyes, eyeAreaSize);
                Rectangle possibleROI_leftEye = new Rectangle(startingLeftEyePointOptimized, eyeAreaSize);

                Point mouthLeftUpPoint = new Point(face.rect.X + face.rect.Width / 5+10, face.rect.Bottom - face.rect.Height/3);
                Point mouthRightUpPoint = new Point(face.rect.X + (face.rect.Width/10)*7 , face.rect.Bottom - face.rect.Height/3);
                Size mouthArea = new Size(face.rect.Width / 2, (face.rect.Height * 2 / 9));
                Point mouthLeftdownPoint = new Point(face.rect.X + face.rect.Width / 5+10, mouthLeftUpPoint.Y+mouthArea.Height );
                Point mouthRightdownPoint = new Point(face.rect.X + (face.rect.Width / 10) * 7, mouthLeftUpPoint.Y + mouthArea.Height);

                //draw mouth area
                if(cb_showMouth.Checked)
                {
                    frame.Draw(new LineSegment2D(mouthLeftUpPoint, mouthRightUpPoint), new Bgr(Color.Chartreuse),2);
                    frame.Draw(new LineSegment2D(mouthLeftUpPoint, mouthLeftdownPoint), new Bgr(Color.Chartreuse), 2);
                    frame.Draw(new LineSegment2D(mouthRightUpPoint, mouthRightdownPoint), new Bgr(Color.Chartreuse), 2);
                    frame.Draw(new LineSegment2D(mouthLeftdownPoint, mouthRightdownPoint), new Bgr(Color.Chartreuse), 2);
                }

                Rectangle possibleROI_mouth=new Rectangle(mouthLeftUpPoint,mouthArea );

                Point noseLeftUpPoint = new Point(face.rect.X + face.rect.Width / 4, face.rect.Top + (face.rect.Height / 12)*5);
                Point noseRightUpPoint = new Point(face.rect.Right - face.rect.Width / 4-10, face.rect.Top + (face.rect.Height / 12) * 5);
                Point noseLeftdownPoint = new Point(face.rect.X + face.rect.Width / 4, face.rect.Bottom - (face.rect.Height /16)*5);
                Point noseRightdownPoint = new Point(face.rect.Right - face.rect.Width / 4-10, face.rect.Bottom - (face.rect.Height / 16) * 5);
                Size noseArea = new Size(face.rect.Right - face.rect.X - face.rect.Width / 2, face.rect.Bottom - (face.rect.Height / 4) - face.rect.Top - (face.rect.Height / 12) * 5);
                //draw nose area
                if (cb_showNose.Checked)
                {
                    frame.Draw(new LineSegment2D(noseLeftUpPoint, noseRightUpPoint), new Bgr(Color.Olive), 1);
                    frame.Draw(new LineSegment2D(noseLeftUpPoint, noseLeftdownPoint), new Bgr(Color.Olive), 1);
                    frame.Draw(new LineSegment2D(noseRightUpPoint, noseRightdownPoint), new Bgr(Color.Olive), 1);
                    frame.Draw(new LineSegment2D(noseLeftdownPoint, noseRightdownPoint), new Bgr(Color.Olive), 1);
                }

                Rectangle possibleROI_nose = new Rectangle(noseLeftUpPoint, noseArea);
                globleROI_mouthRefer = possibleROI_nose;//camshift校正
                frame.ROI = possibleROI_nose;
                grayFrame.ROI = possibleROI_nose;
                pb_roi_nose.Image = grayFrame.Bitmap;

                grayFrame.ROI = Rectangle.Empty;
                //Image<Bgr, Byte> cropnose = new Image<Bgr, byte>(possibleROI_nose.Width, possibleROI_nose.Height);
                //CvInvoke.cvCopy(frame, cropnose, IntPtr.Zero);
                //CvInvoke.cvShowImage("CROPNOSE",cropnose);
                frame.ROI=Rectangle.Empty;
                #endregion

                #region Drawing Utilities
                if (cb_showEyes.Checked)
                {
                    // Let's draw our search area, first the upper line
                    frame.Draw(new LineSegment2D(startingPointSearchEyes, endingPointSearchEyes), new Bgr(Color.White), 3);
                    // draw the bottom line
                    frame.Draw(new LineSegment2D(lowerEyesPointOptimized, new Point((lowerEyesPointOptimized.X + face.rect.Width), (yCoordStartSearchEyes + searchEyesAreaSize.Height))), new Bgr(Color.White), 3);
                    // draw the eyes search vertical line
                    frame.Draw(new LineSegment2D(startingLeftEyePointOptimized, new Point(startingLeftEyePointOptimized.X, (yCoordStartSearchEyes + searchEyesAreaSize.Height))), new Bgr(Color.White), 3);

                    MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_TRIPLEX, 0.6d, 0.6d);
                    frame.Draw("Search Eyes Area", ref font, new Point((startingLeftEyePointOptimized.X - 80), (yCoordStartSearchEyes + searchEyesAreaSize.Height + 15)), new Bgr(Color.Yellow));
                    frame.Draw("Left Eye Area", ref font, new Point(startingPointSearchEyes.X, startingPointSearchEyes.Y - 10), new Bgr(Color.Yellow));
                    frame.Draw("Right Eye Area", ref font, new Point(startingLeftEyePointOptimized.X + searchEyesAreaSize.Height / 2, startingPointSearchEyes.Y - 10), new Bgr(Color.Yellow));


                    //// Let's draw our search area, first the upper line
                    //grayFrame.Draw(new LineSegment2D(startingPointSearchEyes, endingPointSearchEyes), new Gray(1), 3);
                    //// draw the bottom line
                    //grayFrame.Draw(new LineSegment2D(lowerEyesPointOptimized, new Point((lowerEyesPointOptimized.X + face.rect.Width), (yCoordStartSearchEyes + searchEyesAreaSize.Height))), new Gray(1), 3);
                    //// draw the eyes search vertical line
                    //grayFrame.Draw(new LineSegment2D(startingLeftEyePointOptimized, new Point(startingLeftEyePointOptimized.X, (yCoordStartSearchEyes + searchEyesAreaSize.Height))), new Gray(1), 3);

                    ////MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_TRIPLEX, 0.6d, 0.6d);
                    //grayFrame.Draw("Search Eyes Area", ref font, new Point((startingLeftEyePointOptimized.X - 80), (yCoordStartSearchEyes + searchEyesAreaSize.Height + 15)), new Gray(1));
                    //grayFrame.Draw("Left Eye Area", ref font, new Point(startingPointSearchEyes.X, startingPointSearchEyes.Y - 10), new Gray(1));
                    //grayFrame.Draw("Right Eye Area", ref font, new Point(startingLeftEyePointOptimized.X + searchEyesAreaSize.Height / 2, startingPointSearchEyes.Y - 10), new Gray(1));
                    //pb_handled.Image = grayFrame.Bitmap;
                }
                 #endregion
                
                imageHSVDest.ROI = possibleROI_mouth;
                grayFrame.ROI = possibleROI_leftEye;
                //pb_handled.Image = imageHSVDest.Bitmap;
                MCvAvgComp[][] leftEyesDetected = grayFrame.DetectHaarCascade(_faceyes, 1.15, 0, Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20));
                CvInvoke.cvThreshold(imageHSVDest, imageHSVDest, 2, 255, Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY);
               // pb_handled.Image = imageHSVDest.Bitmap;
                //H_MAX = 143;
                //H_MIN = 0;
                //S_MAX = 159;
                //S_MIN = 0;
                //V_MAX = 93;
                //V_MIN = 0;

                #region mouth locating

                grayFrame.ROI = possibleROI_mouth;

                pb_roi_mouth.Image = grayFrame.Bitmap;

                Gray gmin = new Emgu.CV.Structure.Gray(30);
                Gray gmax = new Emgu.CV.Structure.Gray(31);
                //Gray gmin = new Emgu.CV.Structure.Gray(graymin);
                //Gray gmax = new Emgu.CV.Structure.Gray(graymax);
                //Gray gmin = new Emgu.CV.Structure.Gray(Convert.ToDouble( numericUpDown_gmin.Value));
                //Gray gmax = new Emgu.CV.Structure.Gray(Convert.ToDouble(numericUpDown_gmax.Value));
                 grayFrame.InRange(gmin, gmax);
                //pb_handled.Image = grayFrame.Bitmap;
                 CvInvoke.cvThreshold(grayFrame, grayFrame, Convert.ToInt32(numericUpDowngmin.Value), Convert.ToInt32(numericUpDowngmax.Value), Emgu.CV.CvEnum.THRESH.CV_THRESH_TOZERO_INV);
               //CvInvoke.cvThreshold(grayFrame, grayFrame, 96, 209, Emgu.CV.CvEnum.THRESH.CV_THRESH_TOZERO_INV);
                //CvInvoke.cvShowImage("roi_mouth",grayFrame);

                MCvMoments moment = grayFrame.GetMoments(true);
                double m00, m10, m01;
                m00 = moment.m00;
                m10 = moment.m10 ;
                m01 = moment.m01;
                MCvPoint2D64f point2d64f;
                point2d64f = moment.GravityCenter;
                PointF relateCenter = new PointF((float)(m10 / m00), (float)(m01 / m00));
                PointF realCenter = new PointF(relateCenter.X + mouthLeftUpPoint.X, relateCenter.Y + mouthLeftUpPoint.Y);
                //PointF noseCenter = new PointF(relateCenter.X + mouthLeftUpPoint.X+10, relateCenter.Y + mouthLeftUpPoint.Y-45);
      
                //imageHSVDest.Draw(new Cross2DF(relateCenter, 22, 22), new Gray(0), 1);//画个十字
                //grayFrame.Draw(new Cross2DF(realCenter, 22, 22), new Gray(0), 3);//画个十字
               frame.Draw(new Cross2DF(realCenter, 22, 22), new Bgr(Color.DarkOrange), 2);
                if (realCenter.X > 0 && realCenter.Y > 0)
                {
                    tb_mouth.Text = "";
                    tb_mouth.Text = "(" + Convert.ToInt32(realCenter.X) + "," + Convert.ToInt32(realCenter.Y) + ")";
                }

               // frame.Draw(new Cross2DF(noseCenter, 8, 8), new Bgr(Color.Red ), 4);
                moment = grayFrame.GetMoments(false);
                //moment = CvInvoke.cvMoments(imageHSVDest,)              
                grayFrame.ROI = Rectangle.Empty;
               #endregion

                grayFrame.ROI = possibleROI_rightEye;
                MCvAvgComp[][] rightEyesDetected = grayFrame.DetectHaarCascade(_faceyes, 1.15, 0, Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20));
               
                //grayFrame.ROI = Rectangle.Empty;
                //If we are able to find eyes inside the possible face, it should be a face, maybe we find also a couple of eyes
                if (leftEyesDetected[0].Length != 0 && rightEyesDetected[0].Length != 0)
                {
                    //draw the face
                    frame.Draw(face.rect, new Bgr(Color.Violet), 2);
                    
                    #region Hough Circles Eye Detection                                       
                    CvInvoke.cvNot(imageHSV, imageHSV);
                    grayFrame = imageHSV.Convert<Gray, Byte>();
                    if (cb_revert.Checked)
                    {
                        pb_handled.Image = grayFrame.Bitmap;
                    }
                    else
                    {
                        pb_handled.Image = null;
                    }
                   
                    grayFrame.ROI = possibleROI_leftEye;
                    grayFrame._EqualizeHist();
                    grayFrame._EqualizeHist();
                    grayFrame._EqualizeHist();
                    grayFrame._EqualizeHist();
                    //CvInvoke.cvShowImage("均衡化",grayFrame);
                  

                   // CvInvoke.cvSmooth(grayFrame, grayFrame, Emgu.CV.CvEnum.SMOOTH_TYPE.CV_GAUSSIAN ,3, 3, 1, 0);
                    
                   CvInvoke.cvThreshold(grayFrame, grayFrame, 180,248, Emgu.CV.CvEnum.THRESH.CV_THRESH_TOZERO_INV);
                  // CvInvoke.cvShowImage("二值化", grayFrame);
                   pb_roi_eye.Image = grayFrame.Bitmap;
                   CvInvoke.cvSmooth(grayFrame, grayFrame, Emgu.CV.CvEnum.SMOOTH_TYPE.CV_MEDIAN, 3, 3, 1, 0);
                   // grayFrame._EqualizeHist();
                    //CvInvoke.cvErode(grayFrame, grayFrame, IntPtr.Zero, 1);
                    CvInvoke.cvSmooth(grayFrame, grayFrame, Emgu.CV.CvEnum.SMOOTH_TYPE.CV_GAUSSIAN, 3, 3, 1, 0);
                   // CvInvoke.cvShowImage("高斯滤波", grayFrame);
                    //grayFrame._EqualizeHist();
                    //CvInvoke.cvNot(grayFrame, grayFrame);
                   // CvInvoke.cvCanny(grayFrame, grayFrame,254+1,253+2,3);

                   // pb_handled.Image = grayFrame.Bitmap;
                    //CvInvoke.cvThreshold(grayFrame, grayFrame, 30, 250, Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY);
                    //CircleF[] leftEyecircles = grayFrame.HoughCircles(new Gray(180), new Gray(70), 5.0, 10.0, 1, 200)[0];
                    ///CircleF[] leftEyecircles = grayFrame.HoughCircles(new Gray(255), new Gray(250), V_MIN+1, 1, 10, 40)[0];
                    CircleF[] leftEyecircles = grayFrame.HoughCircles(new Gray(180), new Gray(70), 5 , 10, 5, 25)[0];
                  /////  CircleF[] leftEyecircles = grayFrame.HoughCircles(new Gray(180), new Gray(90),4.9,1,V_MAX+2,30)[0];
                    //CvInvoke.cvThreshold(grayFrame, imageHSVDest, 30, 250, Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY);
                   // CvInvoke.cvErode(imageHSVDest, imageHSVDest, IntPtr.Zero, 1);
                    //CvInvoke.cvSmooth(imageHSVDest, imageHSVDest, Emgu.CV.CvEnum.SMOOTH_TYPE.CV_GAUSSIAN, 3, 3, 0, 0);
                   // CvInvoke.cvDilate(imageHSVDest, imageHSVDest, IntPtr.Zero, 2);
                    //CvInvoke.cvSmooth(imageHSVDest, imageHSVDest, Emgu.CV.CvEnum.SMOOTH_TYPE.CV_GAUSSIAN ,3, 3, 0, 0);
                    //leftEyecircles = imageHSVDest.HoughCircles(new Gray(180), new Gray(70),10,35, 1, 30)[0];
                    grayFrame.ROI = Rectangle.Empty;
                   //////// houghResult(leftEyecircles, startingLeftEyePointOptimized, frame);

                  //  pb_handled.Image = grayFrame.Bitmap;

                    grayFrame.ROI = possibleROI_rightEye;
                    //pb_roi_eye.Image = grayFrame.Bitmap;
                    CircleF[] rightEyecircles = grayFrame.HoughCircles(new Gray(180), new Gray(70), 5.0, 10.0, 5, 25)[0];
                     leftEyecircles = imageHSVDest.HoughCircles(new Gray(180), new Gray(70), 5.0, 20.0, 5, 0)[0];
                    grayFrame.ROI = Rectangle.Empty;

                   ///////houghResult(rightEyecircles, startingPointSearchEyes,frame);

                    #endregion
                    
                    //Uncomment this to draw all  eyes' location
                    //we have asssumed there only 1 person detected,so we calculate the mean value
                   markEyes(leftEyesDetected, startingLeftEyePointOptimized, frame, 1);
                   markEyes(rightEyesDetected, startingPointSearchEyes, frame,0);

    //                foreach (MCvAvgComp eyeRight in rightEyesDetected[0])
    //                {
    //                    Rectangle eyeRect = eyeRight.rect;
    //                    eyeRect.Offset(startingPointSearchEyes.X, startingPointSearchEyes.Y);
    //                    CvInvoke.cvRectangle(frame, new Point(eyeRect.X + eyeRect.Width / 2, eyeRect.Y + eyeRect.Height / 2), new Point(eyeRect.X + eyeRect.Width / 2, eyeRect.Y + eyeRect.Height / 2),
    //new Bgr(Color.Yellow ).MCvScalar, 6, LINE_TYPE.FOUR_CONNECTED, 0);
    //                   // frame.Draw(eyeRect, new Bgr(Color.Red), 2);
    //                }   
                 
                }


                watch.Stop();
                System.Console.Out.WriteLine("Detect Time:"+ watch.ElapsedMilliseconds +"ms");
                imageBoxCapturedFrame.Image = frame;

                #region nose tracking
                trackbox = new MCvBox2D();
                trackcomp = new MCvConnectedComp();
                hue = new Image<Gray, byte>(grayFrame.Height,grayFrame.Width );
                //pb_handled.Image = hue.Bitmap;
                hue._EqualizeHist();
                mask = new Image<Gray, byte>(grayFrame.Height, grayFrame.Width);
                hist = new DenseHistogram(30, new RangeF(0, 180));
                backproject = new Image<Gray, byte>(grayFrame.Height, grayFrame.Width);

                // Assign Object's ROI from source image.
                trackingWindow = possibleROI_nose;
        
                // Producing Object's hist
                CalObjectHist(frame);
                Rectangle noseTrackresult = Tracking(frame);
               frame.Draw(noseTrackresult, new Bgr(Color.ForestGreen), 2);
                tb_nose.Text="("+Convert.ToInt16(noseTrackresult.X+noseTrackresult.Width/2)+","+Convert.ToInt16(noseTrackresult.Y+noseTrackresult.Height/2)+")";
                Image<Bgr, Byte> test = new Image<Bgr, byte>(noseTrackresult.Width, noseTrackresult.Height);
                frame.ROI = noseTrackresult;
                CvInvoke.cvCopy(frame, test, IntPtr.Zero);
               // CvInvoke.cvShowImage("test", test);
                frame.ROI = Rectangle.Empty;

                #endregion
            }

            }
           
        }
        private void houghResult(CircleF[] Eyescircles, Point startingReferPoint, Image<Bgr, Byte> frame)
        {

            int idx = 0;
            float sumx = 0;
            float sumy = 0;
            float r = 0;
            if (Eyescircles.Count() > 0)
            {
                foreach (CircleF circle in Eyescircles)
                {
                    idx++;
                    float x = circle.Center.X + startingReferPoint.X;
                    float y = circle.Center.Y + startingReferPoint.Y;
                    // frame.Draw(new CircleF(new PointF(x, y), circle.Radius), new Bgr(Color.RoyalBlue), 4);
                    sumx = sumx + x;
                    sumy = sumy + y;
                    r = circle.Radius + r;
                }
                frame.Draw(new CircleF(new PointF(sumx / idx, sumy / idx), r / (idx + 1)), new Bgr(Color.RoyalBlue), 4);

            }
        }

        private void markEyes(MCvAvgComp[][] eyesDetected, Point startingReferPoint, Image<Bgr, Byte> frame,int leftorright)
        {
            //Uncomment this to draw all rectangle eyes
            //we have asssumed there only 1 person detected,so we calculate the mean value
            int idx = 0;
            float sumx = 0;
            float sumy = 0;

            foreach (MCvAvgComp eyeLeft in eyesDetected[0])
            {
                idx++;
                Rectangle eyeRect = eyeLeft.rect;
                eyeRect.Offset(startingReferPoint.X, startingReferPoint.Y);
                //CvInvoke.cvRectangle(frame, new Point(eyeRect.X + eyeRect.Width / 2, eyeRect.Y + eyeRect.Height / 2), new Point(eyeRect.X + eyeRect.Width / 2 + 4, eyeRect.Y + eyeRect.Height / 2 + 4),
                //   new Bgr(Color.Gold).MCvScalar, 2, LINE_TYPE.FOUR_CONNECTED, 0);
                //CvInvoke.cvRectangle(frame, new Point(eyeRect.X + eyeRect.Width / 2, eyeRect.Y + eyeRect.Height / 2), new Point(eyeRect.X + eyeRect.Width / 2, eyeRect.Y + eyeRect.Height / 2),
                //   new Bgr(Color.Yellow).MCvScalar, 6, LINE_TYPE.FOUR_CONNECTED, 0);
                //frame.Draw(eyeRect, new Bgr(Color.Red), 1);
                sumx = sumx + eyeRect.X + eyeRect.Width / 2;
                sumy = sumy + eyeRect.Y + eyeRect.Height / 2;
            }
            PointF meanLeft = new PointF(sumx / idx, sumy / idx);
            System.Console.WriteLine(meanLeft.X + " , " + meanLeft.Y);
           // frame.Draw(new Cross2DF(meanLeft, 18, 18), new Bgr(Color.DeepPink), 2);
            frame.Draw(new CircleF(meanLeft, 10), new Bgr(Color.Red), 2);
            if (leftorright == 0)
            {
                tb_lefteye.Text = "";
                tb_lefteye.Text = "(" + Convert.ToInt16(meanLeft.X) + "," + Convert.ToInt16(meanLeft.Y) + ")";
            }
            else
            {
                tb_righteye.Text = "";
                tb_righteye.Text="(" + Convert.ToInt16(meanLeft.X) + "," + Convert.ToInt16(meanLeft.Y) + ")";
            }

        }



        public Image<Hsv, Byte> hsv;
        public Image<Gray, Byte> hue;
        public Image<Gray, Byte> mask;
        public Image<Gray, Byte> backproject;
        public DenseHistogram hist;
        private Rectangle trackingWindow;
        private MCvConnectedComp trackcomp;
        private MCvBox2D trackbox;

        public Rectangle Tracking(Image<Bgr, Byte> image)
        {
            UpdateHue(image);

            // Calucate BackProject
            backproject = hist.BackProject(new Image<Gray, Byte>[] { hue });

            // Apply mask
            backproject._And(mask);
            //CvInvoke.cvShowImage("bp", backproject);
            // Tracking windows empty means camshift lost bounding-box last time
            // here we give camshift a new start window from 0,0 (you could change it)
            if (trackingWindow.IsEmpty || trackingWindow.Width == 0 || trackingWindow.Height == 0)
            {
                trackingWindow = new Rectangle(0, 0, 100, 100);
            }
            CvInvoke.cvCamShift(backproject, trackingWindow,
                new MCvTermCriteria(10, 1), out trackcomp, out trackbox);

            trackcomp.rect.Location = new Point(globleROI_mouthRefer.X + globleROI_mouthRefer.Width / 4, globleROI_mouthRefer.Y + globleROI_mouthRefer.Height / 4);
            trackcomp.rect.Size = new Size(trackcomp.rect.Width / 4, trackcomp.rect.Height/4);
            // update tracking window
            trackingWindow = trackcomp.rect;

            return trackingWindow;
        }
        public void ObjectTracking(Image<Bgr, Byte> image, Rectangle ROI)
        {
            // Initialize parameters
            
        }

        private void CalObjectHist(Image<Bgr, Byte> image)
        {
            UpdateHue(image);

            // Set tracking object's ROI
            hue.ROI = trackingWindow;
            mask.ROI = trackingWindow;
            hist.Calculate(new Image<Gray, Byte>[] { hue }, false, mask);

            // Scale Historgram
            float max = 0, min = 0, scale = 0;
            int[] minLocations, maxLocations;
            hist.MinMax(out min, out max, out minLocations, out maxLocations);
            if (max != 0)
            {
                scale = 255 / max;
            }
            CvInvoke.cvConvertScale(hist.MCvHistogram.bins, hist.MCvHistogram.bins, scale, 0);
            
            // Clear ROI
            hue.ROI = System.Drawing.Rectangle.Empty;
            mask.ROI = System.Drawing.Rectangle.Empty;

            // Now we have Object's Histogram, called hist.
        }

        private void UpdateHue(Image<Bgr, Byte> image)
        {
            // release previous image memory
            if (hsv != null) hsv.Dispose();
            hsv = image.Convert<Hsv, Byte>();
           // CvInvoke.cvShowImage("havcamshift",hsv);
            // Drop low saturation pixels
            mask = hsv.Split()[1].ThresholdBinary(new Gray(60), new Gray(255));
            //CvInvoke.cvInRangeS(hsv, new MCvScalar(0, 30, Math.Min(10, 255), 0),
            //    new MCvScalar(180, 256, Math.Max(10, 255), 0), mask);
            CvInvoke.cvInRangeS(hsv, new MCvScalar(H_MIN , S_MIN , V_MIN),
    new MCvScalar(H_MAX, S_MAX ,V_MAX),mask );

            // Get Hue
            hue = hsv.Split()[0];
           // CvInvoke.cvShowImage("hue", hue);
            //CvInvoke.cvShowImage("mask", mask);
        }


        private void imageBoxCapturedFrame_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        #region trackBar
        private void trackBarHmax_Scroll(object sender, EventArgs e)
        {
            H_MAX = trackBarHmax.Value;
            lbHmax.Text="Hmax:" + H_MAX.ToString();
        }

        private void trackBarHmin_Scroll(object sender, EventArgs e)
        {
            H_MIN = trackBarHmin.Value;
            lbHmin.Text = "Hmin" + H_MIN.ToString();
        }

        private void trackBarSmax_Scroll(object sender, EventArgs e)
        {
            S_MAX = trackBarSmax.Value;
            lbSmax.Text  = "Smax" + S_MAX.ToString();
        }

        private void trackBarSmin_Scroll(object sender, EventArgs e)
        {
            S_MIN = trackBarSmin.Value;
            lbSmin .Text  = "Smin" + S_MIN.ToString();
        }

        private void trackBarVmax_Scroll(object sender, EventArgs e)
        {
            V_MAX = trackBarVmax.Value;
            lbVmax.Text  = "Vmax" + V_MAX.ToString();
        }

        private void trackBarVmin_Scroll(object sender, EventArgs e)
        {
            V_MIN = trackBarVmin.Value;
            lbVmin .Text = "Vmin" + V_MIN.ToString();
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {

            enableFlag = !enableFlag;
            if(enableFlag== true)
            {
                button1.Text = "Pause";
            }
            else
            {
                button1.Text = "Start";
            }

        }

        private void trackBar_gmin_Scroll(object sender, EventArgs e)
        {
            //graymin = trackBar_gmin.Value;
            //lb_gmin.Text="gmin:" + graymin.ToString();
        }

        private void trackBar_gmax_Scroll(object sender, EventArgs e)
        {
            //graymax = trackBar_gmax.Value;
            //lb_gmax.Text = "gmax:" + graymax.ToString();
        }

    }
}
