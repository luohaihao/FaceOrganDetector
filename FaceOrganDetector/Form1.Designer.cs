﻿namespace EyeDetector
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageBoxCapturedFrame = new Emgu.CV.UI.ImageBox();
            this.trackBarHmax = new System.Windows.Forms.TrackBar();
            this.trackBarHmin = new System.Windows.Forms.TrackBar();
            this.trackBarSmax = new System.Windows.Forms.TrackBar();
            this.trackBarSmin = new System.Windows.Forms.TrackBar();
            this.trackBarVmax = new System.Windows.Forms.TrackBar();
            this.trackBarVmin = new System.Windows.Forms.TrackBar();
            this.lbHmax = new System.Windows.Forms.Label();
            this.lbHmin = new System.Windows.Forms.Label();
            this.lbSmax = new System.Windows.Forms.Label();
            this.lbSmin = new System.Windows.Forms.Label();
            this.lbVmax = new System.Windows.Forms.Label();
            this.lbVmin = new System.Windows.Forms.Label();
            this.pb_HSV = new System.Windows.Forms.PictureBox();
            this.tb_lefteye = new System.Windows.Forms.TextBox();
            this.tb_nose = new System.Windows.Forms.TextBox();
            this.tb_mouth = new System.Windows.Forms.TextBox();
            this.lb_LeftEye = new System.Windows.Forms.Label();
            this.lb_nose = new System.Windows.Forms.Label();
            this.lb_mouth = new System.Windows.Forms.Label();
            this.tb_righteye = new System.Windows.Forms.TextBox();
            this.pb_handled = new System.Windows.Forms.PictureBox();
            this.pb_roi_eye = new System.Windows.Forms.PictureBox();
            this.pb_roi_nose = new System.Windows.Forms.PictureBox();
            this.pb_roi_mouth = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cb_revert = new System.Windows.Forms.CheckBox();
            this.cb_HSV = new System.Windows.Forms.CheckBox();
            this.cb_showMouth = new System.Windows.Forms.CheckBox();
            this.cb_showNose = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cb_showEyes = new System.Windows.Forms.CheckBox();
            this.cb_colorSelecter = new System.Windows.Forms.CheckBox();
            this.lb_gmin = new System.Windows.Forms.Label();
            this.lb_gmax = new System.Windows.Forms.Label();
            this.numericUpDowngmin = new System.Windows.Forms.NumericUpDown();
            this.numericUpDowngmax = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.imageBoxCapturedFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarHmax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarHmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSmax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVmax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_HSV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_eye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_nose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_mouth)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowngmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowngmax)).BeginInit();
            this.SuspendLayout();
            // 
            // imageBoxCapturedFrame
            // 
            this.imageBoxCapturedFrame.Location = new System.Drawing.Point(0, 35);
            this.imageBoxCapturedFrame.Name = "imageBoxCapturedFrame";
            this.imageBoxCapturedFrame.Size = new System.Drawing.Size(460, 366);
            this.imageBoxCapturedFrame.TabIndex = 2;
            this.imageBoxCapturedFrame.TabStop = false;
            this.imageBoxCapturedFrame.Click += new System.EventHandler(this.imageBoxCapturedFrame_Click);
            // 
            // trackBarHmax
            // 
            this.trackBarHmax.Location = new System.Drawing.Point(499, 202);
            this.trackBarHmax.Maximum = 180;
            this.trackBarHmax.Name = "trackBarHmax";
            this.trackBarHmax.Size = new System.Drawing.Size(104, 45);
            this.trackBarHmax.TabIndex = 4;
            this.trackBarHmax.Scroll += new System.EventHandler(this.trackBarHmax_Scroll);
            // 
            // trackBarHmin
            // 
            this.trackBarHmin.Location = new System.Drawing.Point(499, 236);
            this.trackBarHmin.Maximum = 180;
            this.trackBarHmin.Name = "trackBarHmin";
            this.trackBarHmin.Size = new System.Drawing.Size(104, 45);
            this.trackBarHmin.TabIndex = 5;
            this.trackBarHmin.Scroll += new System.EventHandler(this.trackBarHmin_Scroll);
            // 
            // trackBarSmax
            // 
            this.trackBarSmax.Location = new System.Drawing.Point(499, 298);
            this.trackBarSmax.Maximum = 255;
            this.trackBarSmax.Name = "trackBarSmax";
            this.trackBarSmax.Size = new System.Drawing.Size(104, 45);
            this.trackBarSmax.TabIndex = 6;
            this.trackBarSmax.Scroll += new System.EventHandler(this.trackBarSmax_Scroll);
            // 
            // trackBarSmin
            // 
            this.trackBarSmin.Location = new System.Drawing.Point(499, 325);
            this.trackBarSmin.Maximum = 255;
            this.trackBarSmin.Name = "trackBarSmin";
            this.trackBarSmin.Size = new System.Drawing.Size(104, 45);
            this.trackBarSmin.TabIndex = 7;
            this.trackBarSmin.Scroll += new System.EventHandler(this.trackBarSmin_Scroll);
            // 
            // trackBarVmax
            // 
            this.trackBarVmax.Location = new System.Drawing.Point(499, 399);
            this.trackBarVmax.Maximum = 255;
            this.trackBarVmax.Name = "trackBarVmax";
            this.trackBarVmax.Size = new System.Drawing.Size(104, 45);
            this.trackBarVmax.TabIndex = 8;
            this.trackBarVmax.Scroll += new System.EventHandler(this.trackBarVmax_Scroll);
            // 
            // trackBarVmin
            // 
            this.trackBarVmin.Location = new System.Drawing.Point(499, 436);
            this.trackBarVmin.Maximum = 255;
            this.trackBarVmin.Name = "trackBarVmin";
            this.trackBarVmin.Size = new System.Drawing.Size(104, 45);
            this.trackBarVmin.TabIndex = 9;
            this.trackBarVmin.Scroll += new System.EventHandler(this.trackBarVmin_Scroll);
            // 
            // lbHmax
            // 
            this.lbHmax.AutoSize = true;
            this.lbHmax.Location = new System.Drawing.Point(466, 202);
            this.lbHmax.Name = "lbHmax";
            this.lbHmax.Size = new System.Drawing.Size(29, 12);
            this.lbHmax.TabIndex = 10;
            this.lbHmax.Text = "Hmax";
            // 
            // lbHmin
            // 
            this.lbHmin.AutoSize = true;
            this.lbHmin.Location = new System.Drawing.Point(466, 236);
            this.lbHmin.Name = "lbHmin";
            this.lbHmin.Size = new System.Drawing.Size(29, 12);
            this.lbHmin.TabIndex = 11;
            this.lbHmin.Text = "Hmin";
            // 
            // lbSmax
            // 
            this.lbSmax.AutoSize = true;
            this.lbSmax.Location = new System.Drawing.Point(466, 298);
            this.lbSmax.Name = "lbSmax";
            this.lbSmax.Size = new System.Drawing.Size(29, 12);
            this.lbSmax.TabIndex = 12;
            this.lbSmax.Text = "Smax";
            // 
            // lbSmin
            // 
            this.lbSmin.AutoSize = true;
            this.lbSmin.Location = new System.Drawing.Point(466, 331);
            this.lbSmin.Name = "lbSmin";
            this.lbSmin.Size = new System.Drawing.Size(29, 12);
            this.lbSmin.TabIndex = 13;
            this.lbSmin.Text = "Smin";
            // 
            // lbVmax
            // 
            this.lbVmax.AutoSize = true;
            this.lbVmax.Location = new System.Drawing.Point(466, 399);
            this.lbVmax.Name = "lbVmax";
            this.lbVmax.Size = new System.Drawing.Size(29, 12);
            this.lbVmax.TabIndex = 14;
            this.lbVmax.Text = "Vmax";
            // 
            // lbVmin
            // 
            this.lbVmin.AutoSize = true;
            this.lbVmin.Location = new System.Drawing.Point(466, 436);
            this.lbVmin.Name = "lbVmin";
            this.lbVmin.Size = new System.Drawing.Size(29, 12);
            this.lbVmin.TabIndex = 15;
            this.lbVmin.Text = "Vmin";
            // 
            // pb_HSV
            // 
            this.pb_HSV.Location = new System.Drawing.Point(609, 281);
            this.pb_HSV.Name = "pb_HSV";
            this.pb_HSV.Size = new System.Drawing.Size(296, 203);
            this.pb_HSV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_HSV.TabIndex = 16;
            this.pb_HSV.TabStop = false;
            // 
            // tb_lefteye
            // 
            this.tb_lefteye.Location = new System.Drawing.Point(33, 480);
            this.tb_lefteye.Name = "tb_lefteye";
            this.tb_lefteye.Size = new System.Drawing.Size(60, 21);
            this.tb_lefteye.TabIndex = 20;
            // 
            // tb_nose
            // 
            this.tb_nose.Location = new System.Drawing.Point(207, 480);
            this.tb_nose.Name = "tb_nose";
            this.tb_nose.Size = new System.Drawing.Size(100, 21);
            this.tb_nose.TabIndex = 21;
            // 
            // tb_mouth
            // 
            this.tb_mouth.Location = new System.Drawing.Point(360, 480);
            this.tb_mouth.Name = "tb_mouth";
            this.tb_mouth.Size = new System.Drawing.Size(100, 21);
            this.tb_mouth.TabIndex = 22;
            // 
            // lb_LeftEye
            // 
            this.lb_LeftEye.AutoSize = true;
            this.lb_LeftEye.Location = new System.Drawing.Point(31, 456);
            this.lb_LeftEye.Name = "lb_LeftEye";
            this.lb_LeftEye.Size = new System.Drawing.Size(35, 12);
            this.lb_LeftEye.TabIndex = 23;
            this.lb_LeftEye.Text = "Eyes:";
            // 
            // lb_nose
            // 
            this.lb_nose.AutoSize = true;
            this.lb_nose.Location = new System.Drawing.Point(205, 456);
            this.lb_nose.Name = "lb_nose";
            this.lb_nose.Size = new System.Drawing.Size(35, 12);
            this.lb_nose.TabIndex = 24;
            this.lb_nose.Text = "Nose:";
            // 
            // lb_mouth
            // 
            this.lb_mouth.AutoSize = true;
            this.lb_mouth.Location = new System.Drawing.Point(358, 456);
            this.lb_mouth.Name = "lb_mouth";
            this.lb_mouth.Size = new System.Drawing.Size(41, 12);
            this.lb_mouth.TabIndex = 25;
            this.lb_mouth.Text = "Mouth:";
            // 
            // tb_righteye
            // 
            this.tb_righteye.Location = new System.Drawing.Point(99, 480);
            this.tb_righteye.Name = "tb_righteye";
            this.tb_righteye.Size = new System.Drawing.Size(58, 21);
            this.tb_righteye.TabIndex = 26;
            // 
            // pb_handled
            // 
            this.pb_handled.Location = new System.Drawing.Point(609, 35);
            this.pb_handled.Name = "pb_handled";
            this.pb_handled.Size = new System.Drawing.Size(296, 212);
            this.pb_handled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_handled.TabIndex = 3;
            this.pb_handled.TabStop = false;
            // 
            // pb_roi_eye
            // 
            this.pb_roi_eye.Location = new System.Drawing.Point(47, 33);
            this.pb_roi_eye.Name = "pb_roi_eye";
            this.pb_roi_eye.Size = new System.Drawing.Size(119, 106);
            this.pb_roi_eye.TabIndex = 17;
            this.pb_roi_eye.TabStop = false;
            // 
            // pb_roi_nose
            // 
            this.pb_roi_nose.Location = new System.Drawing.Point(47, 150);
            this.pb_roi_nose.Name = "pb_roi_nose";
            this.pb_roi_nose.Size = new System.Drawing.Size(119, 107);
            this.pb_roi_nose.TabIndex = 18;
            this.pb_roi_nose.TabStop = false;
            // 
            // pb_roi_mouth
            // 
            this.pb_roi_mouth.Location = new System.Drawing.Point(47, 301);
            this.pb_roi_mouth.Name = "pb_roi_mouth";
            this.pb_roi_mouth.Size = new System.Drawing.Size(134, 86);
            this.pb_roi_mouth.TabIndex = 19;
            this.pb_roi_mouth.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(607, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 27;
            this.label1.Text = "Revert";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(607, 264);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 12);
            this.label2.TabIndex = 28;
            this.label2.Text = "HSV";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDowngmax);
            this.groupBox1.Controls.Add(this.numericUpDowngmin);
            this.groupBox1.Controls.Add(this.lb_gmax);
            this.groupBox1.Controls.Add(this.lb_gmin);
            this.groupBox1.Controls.Add(this.pb_roi_eye);
            this.groupBox1.Controls.Add(this.pb_roi_nose);
            this.groupBox1.Controls.Add(this.pb_roi_mouth);
            this.groupBox1.Location = new System.Drawing.Point(920, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(232, 484);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Possible Region";
            // 
            // cb_revert
            // 
            this.cb_revert.AutoSize = true;
            this.cb_revert.Location = new System.Drawing.Point(480, 61);
            this.cb_revert.Name = "cb_revert";
            this.cb_revert.Size = new System.Drawing.Size(96, 16);
            this.cb_revert.TabIndex = 31;
            this.cb_revert.Text = "Revert Image";
            this.cb_revert.UseVisualStyleBackColor = true;
            // 
            // cb_HSV
            // 
            this.cb_HSV.AutoSize = true;
            this.cb_HSV.Location = new System.Drawing.Point(480, 83);
            this.cb_HSV.Name = "cb_HSV";
            this.cb_HSV.Size = new System.Drawing.Size(42, 16);
            this.cb_HSV.TabIndex = 32;
            this.cb_HSV.Text = "HSV";
            this.cb_HSV.UseVisualStyleBackColor = true;
            // 
            // cb_showMouth
            // 
            this.cb_showMouth.AutoSize = true;
            this.cb_showMouth.Location = new System.Drawing.Point(480, 105);
            this.cb_showMouth.Name = "cb_showMouth";
            this.cb_showMouth.Size = new System.Drawing.Size(114, 16);
            this.cb_showMouth.TabIndex = 33;
            this.cb_showMouth.Text = "Show Mouth Area";
            this.cb_showMouth.UseVisualStyleBackColor = true;
            // 
            // cb_showNose
            // 
            this.cb_showNose.AutoSize = true;
            this.cb_showNose.Location = new System.Drawing.Point(480, 129);
            this.cb_showNose.Name = "cb_showNose";
            this.cb_showNose.Size = new System.Drawing.Size(108, 16);
            this.cb_showNose.TabIndex = 34;
            this.cb_showNose.Text = "Show Nose Area";
            this.cb_showNose.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(480, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 35;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cb_showEyes
            // 
            this.cb_showEyes.AutoSize = true;
            this.cb_showEyes.Checked = true;
            this.cb_showEyes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_showEyes.Location = new System.Drawing.Point(480, 152);
            this.cb_showEyes.Name = "cb_showEyes";
            this.cb_showEyes.Size = new System.Drawing.Size(102, 16);
            this.cb_showEyes.TabIndex = 36;
            this.cb_showEyes.Text = "Show Eye Area";
            this.cb_showEyes.UseVisualStyleBackColor = true;
            // 
            // cb_colorSelecter
            // 
            this.cb_colorSelecter.AutoSize = true;
            this.cb_colorSelecter.Location = new System.Drawing.Point(480, 174);
            this.cb_colorSelecter.Name = "cb_colorSelecter";
            this.cb_colorSelecter.Size = new System.Drawing.Size(108, 16);
            this.cb_colorSelecter.TabIndex = 37;
            this.cb_colorSelecter.Text = "Color selecter";
            this.cb_colorSelecter.UseVisualStyleBackColor = true;
            // 
            // lb_gmin
            // 
            this.lb_gmin.AutoSize = true;
            this.lb_gmin.Location = new System.Drawing.Point(23, 408);
            this.lb_gmin.Name = "lb_gmin";
            this.lb_gmin.Size = new System.Drawing.Size(35, 12);
            this.lb_gmin.TabIndex = 26;
            this.lb_gmin.Text = "gmin:";
            // 
            // lb_gmax
            // 
            this.lb_gmax.AutoSize = true;
            this.lb_gmax.Location = new System.Drawing.Point(23, 439);
            this.lb_gmax.Name = "lb_gmax";
            this.lb_gmax.Size = new System.Drawing.Size(35, 12);
            this.lb_gmax.TabIndex = 27;
            this.lb_gmax.Text = "gmax:";
            // 
            // numericUpDowngmin
            // 
            this.numericUpDowngmin.Location = new System.Drawing.Point(64, 406);
            this.numericUpDowngmin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDowngmin.Name = "numericUpDowngmin";
            this.numericUpDowngmin.Size = new System.Drawing.Size(73, 21);
            this.numericUpDowngmin.TabIndex = 28;
            this.numericUpDowngmin.Value = new decimal(new int[] {
            96,
            0,
            0,
            0});
            // 
            // numericUpDowngmax
            // 
            this.numericUpDowngmax.Location = new System.Drawing.Point(64, 439);
            this.numericUpDowngmax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDowngmax.Name = "numericUpDowngmax";
            this.numericUpDowngmax.Size = new System.Drawing.Size(73, 21);
            this.numericUpDowngmax.TabIndex = 29;
            this.numericUpDowngmax.Value = new decimal(new int[] {
            209,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 520);
            this.Controls.Add(this.cb_colorSelecter);
            this.Controls.Add(this.cb_showEyes);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cb_showNose);
            this.Controls.Add(this.cb_showMouth);
            this.Controls.Add(this.cb_HSV);
            this.Controls.Add(this.cb_revert);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_righteye);
            this.Controls.Add(this.lb_mouth);
            this.Controls.Add(this.lb_nose);
            this.Controls.Add(this.lb_LeftEye);
            this.Controls.Add(this.tb_mouth);
            this.Controls.Add(this.tb_nose);
            this.Controls.Add(this.tb_lefteye);
            this.Controls.Add(this.pb_HSV);
            this.Controls.Add(this.lbVmin);
            this.Controls.Add(this.lbVmax);
            this.Controls.Add(this.lbSmin);
            this.Controls.Add(this.lbSmax);
            this.Controls.Add(this.lbHmin);
            this.Controls.Add(this.lbHmax);
            this.Controls.Add(this.trackBarVmin);
            this.Controls.Add(this.trackBarVmax);
            this.Controls.Add(this.trackBarSmin);
            this.Controls.Add(this.trackBarSmax);
            this.Controls.Add(this.trackBarHmin);
            this.Controls.Add(this.trackBarHmax);
            this.Controls.Add(this.pb_handled);
            this.Controls.Add(this.imageBoxCapturedFrame);
            this.Name = "Form1";
            this.Text = "EmguCV Facial Organs  Recognition ";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageBoxCapturedFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarHmax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarHmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSmax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVmax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_HSV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_eye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_nose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_roi_mouth)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowngmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowngmax)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Emgu.CV.UI.ImageBox imageBoxCapturedFrame;
        private System.Windows.Forms.TrackBar trackBarHmax;
        private System.Windows.Forms.TrackBar trackBarHmin;
        private System.Windows.Forms.TrackBar trackBarSmax;
        private System.Windows.Forms.TrackBar trackBarSmin;
        private System.Windows.Forms.TrackBar trackBarVmax;
        private System.Windows.Forms.TrackBar trackBarVmin;
        private System.Windows.Forms.Label lbHmax;
        private System.Windows.Forms.Label lbHmin;
        private System.Windows.Forms.Label lbSmax;
        private System.Windows.Forms.Label lbSmin;
        private System.Windows.Forms.Label lbVmax;
        private System.Windows.Forms.Label lbVmin;
        private System.Windows.Forms.PictureBox pb_HSV;
        private System.Windows.Forms.TextBox tb_lefteye;
        private System.Windows.Forms.TextBox tb_nose;
        private System.Windows.Forms.TextBox tb_mouth;
        private System.Windows.Forms.Label lb_LeftEye;
        private System.Windows.Forms.Label lb_nose;
        private System.Windows.Forms.Label lb_mouth;
        private System.Windows.Forms.TextBox tb_righteye;
        private System.Windows.Forms.PictureBox pb_handled;
        private System.Windows.Forms.PictureBox pb_roi_eye;
        private System.Windows.Forms.PictureBox pb_roi_nose;
        private System.Windows.Forms.PictureBox pb_roi_mouth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cb_revert;
        private System.Windows.Forms.CheckBox cb_HSV;
        private System.Windows.Forms.CheckBox cb_showMouth;
        private System.Windows.Forms.CheckBox cb_showNose;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox cb_showEyes;
        private System.Windows.Forms.CheckBox cb_colorSelecter;
        private System.Windows.Forms.Label lb_gmax;
        private System.Windows.Forms.Label lb_gmin;
        private System.Windows.Forms.NumericUpDown numericUpDowngmax;
        private System.Windows.Forms.NumericUpDown numericUpDowngmin;
    }
}

